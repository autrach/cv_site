from django.apps import AppConfig


class MyCvSiteConfig(AppConfig):
    name = 'my_cv_site'

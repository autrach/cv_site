from django.shortcuts import render

def home(request):
    return render(request, 'my_cv_site/home.html')